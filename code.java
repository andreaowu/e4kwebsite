//input: String consisting of only letters
//output: sorted


// Mergesort algorithm
String sort(String s) {
	if (s.length > 1) {
		return merge(sort(s.substring(0, s.length/2)), sort(s.substring(s.length/2, s.length)));
	} else {
		return s;
	}
}

String merge(String x, String y) {
	if (x.length == 0) {
		return y;
	} else if (y.length == 0) {
		return x;
	} else if (x.get(0) <= y.get(0)) {
		return x[0] + merge(x.substring(1), y);
	} else {
		return y[0] + merge(x, y.substring(1));
	}
}

// Quicksort algorithm
String sort(String s) {
	if (s.length <= 1) {
		return s;
	}
	String pivot = Math.max(s[0], s[s.length-1], s[Math.floor(s.length/2)])
	ArrayList<String>() less = new ArrayList<String>();
	ArrayList<String>() greater = new ArrayList<String>();
	for (int i = 0; i < s.length; i++) {
		if (s.charAt <= pivot) {
			less.add(s.charAt);
		} else {
			greater.add(s.charAt);
		}
	}
	return sort(less) + pivot + sort(greater);
}

// DFS
void DFS(Tree t) {
	Stack s = new Stack();
	if (!t.hasChild) {
		return;
	}
	s.push(t.node);
	ArrayList<String>() visited = new ArrayList<String>();
	while (!q.isEmpty()) {
		visit = s.pop();
		visited.add(visit);
		for child of visit:
			s.push(child);
	}
}



//input: String
//output: all permutations of the string

// this is outright wrong
void permutations(String s) {
	for (int i = 0; i < s.length; i++) { //nth letter
		String c = s.charAt(i); // c -> want to move it to the second spot
		for (int j = 0; j < s.length; j++) { // where to move the letter
			System.out.println(s.substring(j, j + 1) + c + s.substring(j + 1)); 
		}
	}
}

// correct
void permutations(String s) {
	ArrayList<String>() index = new ArrayList<String>(){0, 0};
	ArrayList<String>() list = new ArrayList<String>();
	for (int i = 0; i < s.length; i++) {
		index = (index[1], len(list));
		for (int j = 0; j < list.length; j++) {
			for (int k = 0; k < s.length; k++) {
				if ( (list.get(j) + s.charAt(k)).length == s.length ) {
					System.out.println(list.get(j) + s.charAt(k));
				}
			}
		}
	}
}


// input: two lists and an integer n
// output: nth smallest number in union of a1 and 2

// idea: do a binary search on both
a1 = [ 1, 10, 100, 120, 130 ] => a
a2 [ 2, 3, 4, 20, 30 ] => b

n = 1 => 1
n = 6 => 20

a + b = n

nth smallest

int getNSmallest(int[] a1, int[] a2, int n) {
	int a = a1.length/2;
	int b = a2.length/2;
	// using binary search
	if (a1.length == 1 && a2.length == 1) {
		if (a1[0] < a2[0]) {
			return a1[0];
		}
		return a2[0];
	} else if (a1.length == 1) {
		return a1[0];
	} else if (a2.length == 1) {
		return a2[0];
	}
	if (a1[a] <= a2[b]) {
		if (a + b < n) {
			return getNSmallest(a1[a, a1.length], a2[0, b], a + b)
		}
		return getNSmallest(a1[0, a], a2[b, a2.length], a + b)
	}

	// original implementation
	for (int i = 0; i < n; i++) { 
		if (a1[a] <= a2[b]) {
			a++;
		} else {
			b++;
	}
	if (a1[a] <= a2[b]) {
		return a1[a];
	}
	return a2[b];
}

